package nl.ziggo.digitalmedia.api.common.security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;

import org.junit.Test;

public class AccessServiceTest {
	@Test
	public void testFindAccountInParameters() throws Exception {
		AccessServiceImpl controller = new AccessServiceImpl();
		assertNull(controller.parseParameterValue(null, AccessServiceImpl.ACCOUNT_PARAM_NAME));
		assertNull(controller.parseParameterValue("accont=harry.de.lange", AccessServiceImpl.ACCOUNT_PARAM_NAME));
		assertEquals("harry.de.lange", controller.parseParameterValue("account=harry.de.lange", AccessServiceImpl.ACCOUNT_PARAM_NAME));
		assertEquals("harry.de.lange", controller.parseParameterValue("account=harry.de.lange&param2=value2", AccessServiceImpl.ACCOUNT_PARAM_NAME));
		assertEquals("harry.de.lange", controller.parseParameterValue("param0=account&account=harry.de.lange&param2=value2", AccessServiceImpl.ACCOUNT_PARAM_NAME));
		assertEquals("harry.de.lange", controller.parseParameterValue("account=harry.de.lange&&&param2=value2", AccessServiceImpl.ACCOUNT_PARAM_NAME));
	}

	@Test
	public void testTokenInParameters() throws Exception {
		AccessServiceImpl controller = new AccessServiceImpl();
		assertNull(controller.parseParameterValue(null, AccessServiceImpl.TOKEN_PARAM_NAME));
		assertNull(controller.parseParameterValue("tokn=T0ok3Nt#xT", AccessServiceImpl.TOKEN_PARAM_NAME));
		assertEquals("T0ok3Nt#xT", controller.parseParameterValue("token=T0ok3Nt#xT", AccessServiceImpl.TOKEN_PARAM_NAME));
		assertEquals("T0ok3Nt#xT", controller.parseParameterValue("token=T0ok3Nt#xT&p2=asdf", AccessServiceImpl.TOKEN_PARAM_NAME));
		assertEquals("T0ok3Nt#xT", controller.parseParameterValue("p0=qwerty&token=T0ok3Nt#xT&p2=asdf", AccessServiceImpl.TOKEN_PARAM_NAME));
		assertEquals("T0ok3Nt#xT", controller.parseParameterValue("token=T0ok3Nt#xT&&&p2=asdf", AccessServiceImpl.TOKEN_PARAM_NAME));
	}

	@Test
	public void testFindCall() throws Exception {
		AccessServiceImpl controller = new AccessServiceImpl();
		assertNull(controller.findCallFromPath(null));
		assertNull(controller.findCallFromPath("http://api.ziggo.nl/context/rets/restant/ci_plus_tv/brands/samsung/v1.0"));
		assertEquals("/restant/ci_plus_tv/brands/samsung/v1.0", controller.findCallFromPath("http://api.ziggo.nl/context/rest/restant/ci_plus_tv/brands/samsung/v1.0"));
	}

	@Test
	public void generateToken() throws Exception {
		AccessServiceImpl service = new AccessServiceImpl();
		assertNull(service.createSignature(null));

		StringBuilder text = new StringBuilder();
		text.append("apikey=");
		text.append("H8MJGVYVAXZZVSE6AYWA");
		text.append("&timestamp=");
		text.append(1349105615731L);
		text.append("MPVvUWvAAnza9gHXToA3");
		String expectedSignature = "433684FB4815B7EAF6578950688F297DD5C3203A3CB75EDBD74494EC591312D4";
		assertEquals(expectedSignature, service.createSignature(text.toString()));
	}

	@Test
	public void checkEmptyTimestamp() throws Exception {
		AccessServiceImpl service = new AccessServiceImpl();
		assertFalse(service.checkTimestamp(null));
	}

	@Test
	public void checkFutureTimestamp() throws Exception {
		AccessServiceImpl service = new AccessServiceImpl();
		Calendar clock = Calendar.getInstance();
		clock.add(Calendar.SECOND, 20);
		long timestampSeconds = clock.getTimeInMillis() / 1000;
		assertFalse(service.checkTimestamp(Long.toString(timestampSeconds)));
	}

	@Test
	public void checkOldTimestamp() throws Exception {
		AccessServiceImpl service = new AccessServiceImpl();
		Calendar clock = Calendar.getInstance();
		clock.add(Calendar.HOUR, -1);
		clock.add(Calendar.SECOND, -20);
		long timestampSeconds = clock.getTimeInMillis() / 1000;
		assertFalse(service.checkTimestamp(Long.toString(timestampSeconds)));
	}

	@Test
	public void checkFineTimestamp() throws Exception {
		AccessServiceImpl service = new AccessServiceImpl();
		Calendar clock = Calendar.getInstance();
		clock.add(Calendar.MINUTE, -15);
		long timestampSeconds = clock.getTimeInMillis() / 1000;
		assertTrue(service.checkTimestamp(Long.toString(timestampSeconds)));
	}

	@Test
	public void checkOldTokenHasExpired() throws Exception {
		AccessServiceImpl service = new AccessServiceImpl();
		Calendar clock = Calendar.getInstance();
		clock.add(Calendar.HOUR, -4);
		clock.add(Calendar.MINUTE, -15);
		long timestampSeconds = clock.getTimeInMillis() / 1000;
		assertTrue(service.hasTokenExpired(timestampSeconds));
	}

	@Test
	public void checkNewRecentTokenHasNotExpired() throws Exception {
		AccessServiceImpl service = new AccessServiceImpl();
		Calendar clock = Calendar.getInstance();
		clock.add(Calendar.HOUR, -3);
		clock.add(Calendar.MINUTE, -15);
		long timestampSeconds = clock.getTimeInMillis() / 1000;
		assertFalse(service.hasTokenExpired(timestampSeconds));
	}

	@Test
	public void checkFutureRecentTokenHasNotExpired() throws Exception {
		AccessServiceImpl service = new AccessServiceImpl();
		Calendar clock = Calendar.getInstance();
		clock.add(Calendar.HOUR, 3);
		long timestampSeconds = clock.getTimeInMillis() / 1000;
		assertFalse(service.hasTokenExpired(timestampSeconds));
	}

	@Test
	public void testGenerateToken() throws Exception {
		AccessServiceImpl service = new AccessServiceImpl();
		String token = service.generateToken();
		assertNotNull(token);
		assertEquals(64, token.length());
	}
}