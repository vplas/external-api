package nl.ziggo.digitalmedia.api.common.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;

import nl.ziggo.digitalmedia.api.common.persistence.AccessPath;
import nl.ziggo.digitalmedia.api.common.persistence.Client;
import nl.ziggo.digitalmedia.api.common.persistence.Token;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ClientAccessDaoImplTest {
	private ClassPathXmlApplicationContext context;
	private ClientAccessDao accessDao;

	@Before
	public void setUp() {
		String[] contextFiles = new String[] {
				"applicationContext.xml"
		};
		context = new ClassPathXmlApplicationContext(contextFiles);
		assertNotNull(context);

		accessDao = (ClientAccessDao) context.getBean("accessDao");
		assertNotNull(accessDao);

		addPersistentClients();
		addPersistentTokens();
		addPersistentAccessPaths();;
	}

	@After
	public void cleanUp() {
		deletePersistentClients();
		deletePersistentTokens();
		deletePersistentAccessPaths();
	}

	@Test
	public void existClient() throws Exception {
		assertFalse(accessDao.existClient(null, null));
		assertFalse(accessDao.existClient("192.168.3.3", "AA-BB-CC-3"));
		assertTrue(accessDao.existClient("AA-BB-CC-3", "192.168.3.3"));
		assertFalse(accessDao.existClient("192.168.1.2", "AA-BB-CC-3"));
	}

	@Test
	public void retrieveSecret() throws Exception {
		assertNull(accessDao.retrieveSecretByClient(null));
		assertNull(accessDao.retrieveSecretByClient("fakeClient"));
		assertEquals("ASD678SDF890HJK", accessDao.retrieveSecretByClient("AA-BB-CC-2"));		
	}

	@Test
	public void fetchToken() throws Exception {
		assertNull(accessDao.fetchTokenByClient(null, null));
		assertNull(accessDao.fetchTokenByClient("123876EAFCC", "AA-BB-CC-2"));
		assertNull(accessDao.fetchTokenByClient("AA-BB-CC-2", "AA-BB-CC-2"));
		Token token = accessDao.fetchTokenByClient("AA-BB-CC-2", "123876EAFCC");
		assertNotNull(token);
		assertEquals(2L, token.getId());
		assertEquals("AA-BB-CC-2", token.getClientCode());
		assertEquals("123876EAFCC", token.getToken());
		assertEquals(1388529224L, token.getTimestamp());
		assertEquals(1388571193L, token.getLastAccessTime());
	}

	@Test
	public void hasAccess() throws Exception {
		assertFalse(accessDao.existAccessPath(null, null));
		assertFalse(accessDao.existAccessPath("", ""));
		assertFalse(accessDao.existAccessPath("AA-BB-CC-1", "/tv/path10"));
		assertTrue(accessDao.existAccessPath("AA-BB-CC-1", "/tv/path1"));
	}

	private void addPersistentClients() {
		Client client1 = new Client();
		client1.setId(1L);
		client1.setName("company 1");
		client1.setSourceIpAddress("192.168.1.1");
		client1.setCode("AA-BB-CC-1");
		client1.setSecret("$#k*1d");
		accessDao.persist(client1);

		Client client2 = new Client();
		client2.setId(2L);
		client2.setName("company 2");
		client2.setSourceIpAddress("192.168.1.2");
		client2.setCode("AA-BB-CC-2");
		client2.setSecret("ASD678SDF890HJK");
		accessDao.persist(client2);

		Client client3 = new Client();
		client3.setId(3L);
		client3.setName("company 3");
		client3.setSourceIpAddress("192.168.3.3");
		client3.setCode("AA-BB-CC-3");
		client3.setSecret("$%^&*(DFGHJK56789");
		accessDao.persist(client3);

		Client client4 = new Client();
		client4.setId(4L);
		client4.setName("company 4");
		client4.setSourceIpAddress("192.168.1.4");
		client4.setCode("AA-BB-CC-4");
		client4.setSecret("UYTTUUYDFDF");
		accessDao.persist(client4);

		Client client5 = new Client();
		client5.setId(5L);
		client5.setName("company 5");
		client5.setSourceIpAddress("192.168.1.5");
		client5.setCode("AA-BB-CC-5");
		client5.setSecret("SDFJHKJHSDF*()@#");
		accessDao.persist(client5);
	}

	private void addPersistentTokens() {
		Calendar clock = Calendar.getInstance();
		Token token1 = new Token();
		token1.setId(1L);
		token1.setClientCode("AA-BB-CC-1");
		token1.setToken("ADF789234BCE");
		clock.set(2013, 11, 31, 23, 34, 45);
		token1.setTimestamp(clock.getTimeInMillis() / 1000);
		clock.set(2014, 0, 1, 11, 12, 13);
		token1.setLastAccessTime(clock.getTimeInMillis() / 1000);
		accessDao.persist(token1);

		Token token2 = new Token();
		token2.setId(2L);
		token2.setClientCode("AA-BB-CC-2");
		token2.setToken("123876EAFCC");
		clock.set(2013, 11, 31, 23, 33, 44);
		token2.setTimestamp(clock.getTimeInMillis() / 1000);
		clock.set(2014, 0, 1, 11, 13, 13);
		token2.setLastAccessTime(clock.getTimeInMillis() / 1000);
		accessDao.persist(token2);

		Token token3 = new Token();
		token3.setId(3L);
		token3.setClientCode("AA-BB-CC-3");
		token3.setToken("9807BBAA");
		clock.set(2014, 0, 1, 10, 34, 45);
		token3.setTimestamp(clock.getTimeInMillis() / 1000);
		clock.set(2014, 0, 1, 11, 14, 13);
		token3.setLastAccessTime(clock.getTimeInMillis() / 1000);
		accessDao.persist(token3);

		Token token4 = new Token();
		token4.setId(4L);
		token4.setClientCode("AA-BB-CC-4");
		token4.setToken("DDEEB876B23");
		clock.set(2013, 9, 31, 23, 34, 45);
		token4.setTimestamp(clock.getTimeInMillis() / 1000);
		clock.set(2014, 0, 1, 11, 15, 13);
		token4.setLastAccessTime(clock.getTimeInMillis() / 1000);
		accessDao.persist(token4);

		Token token5 = new Token();
		token5.setId(5L);
		token5.setClientCode("AA-BB-CC-5");
		token5.setToken("3254FBEC876AB");
		clock.set(2013, 10, 2, 23, 34, 45);
		token5.setTimestamp(clock.getTimeInMillis() / 1000);
		clock.set(2014, 0, 1, 11, 16, 13);
		token5.setLastAccessTime(clock.getTimeInMillis() / 1000);
		accessDao.persist(token5);
	}

	private void addPersistentAccessPaths() {
		AccessPath accessPath1 = new AccessPath();
		accessPath1.setId(1L);
		accessPath1.setClientCode("AA-BB-CC-5");
		accessPath1.setPath("/tv/path1");
		accessDao.persist(accessPath1);

		AccessPath accessPath2 = new AccessPath();
		accessPath2.setId(2L);
		accessPath2.setClientCode("AA-BB-CC-5");
		accessPath2.setPath("/tv/path3");
		accessDao.persist(accessPath2);

		AccessPath accessPath3 = new AccessPath();
		accessPath3.setId(3L);
		accessPath3.setClientCode("AA-BB-CC-1");
		accessPath3.setPath("/tv/path1");
		accessDao.persist(accessPath3);

		AccessPath accessPath4 = new AccessPath();
		accessPath4.setId(4L);
		accessPath4.setClientCode("AA-BB-CC-1");
		accessPath4.setPath("/tv/path1");
		accessDao.persist(accessPath4);

		AccessPath accessPath5 = new AccessPath();
		accessPath5.setId(5L);
		accessPath5.setClientCode("AA-BB-CC-5");
		accessPath5.setPath("/tv/path2");
		accessDao.persist(accessPath5);
	}

	private void deletePersistentClients() {
		accessDao.deleteAllClients();
	}

	private void deletePersistentTokens() {
		accessDao.deleteAllTokens();
	}

	private void deletePersistentAccessPaths() {
		accessDao.deleteAllAccessPaths();
	}
}