package nl.ziggo.digitalmedia.api.ci_plus_tv.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import nl.ziggo.digitalmedia.api.ci_plus_tv.persistence.TvDto;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CiPlusTvDaoImplTest {
	private ClassPathXmlApplicationContext context;
	private CiPlusTvDao tvDao;

	@Before
	public void setUp() {
		String[] contextFiles = new String[] {
				"applicationContext.xml"
		};
		context = new ClassPathXmlApplicationContext(contextFiles);
		assertNotNull(context);

		tvDao = (CiPlusTvDao) context.getBean("ciPlusTvDao");
		assertNotNull(tvDao);

		addPersistentTvs();
	}

	@After
	public void cleanUp() {
		deletePersistentTvs();
	}

	@Test
	public void persistTv() throws Exception {
		List<TvDto> tvs = tvDao.selectAllTvs();

		assertNotNull(tvs);
		assertEquals(5, tvs.size());
		assertEquals("brand1", tvs.get(0).getBrandName());
		assertEquals("device1", tvs.get(0).getDeviceName());
		assertEquals("type1", tvs.get(0).getTypeNumber());
		assertEquals("111.aaa", tvs.get(0).getSoftwareVersion());
		assertEquals(2001, tvs.get(0).getYear());
		assertTrue(tvs.get(0).isInteractive());
	}

	@Test
	public void getAllTvs() throws Exception {
		List<TvDto> tvs = tvDao.selectAllTvs();
		assertNotNull(tvs);
		assertEquals(5, tvs.size());
		assertEquals("brand2", tvs.get(2).getBrandName());
		assertEquals("device1", tvs.get(2).getDeviceName());
		assertEquals("type1", tvs.get(2).getTypeNumber());
		assertEquals("55.aaa", tvs.get(2).getSoftwareVersion());
		assertEquals(2009, tvs.get(2).getYear());
		assertFalse(tvs.get(2).isInteractive());
	}

	@Test
	public void deleteTv() throws Exception {
		List<TvDto> tvs = tvDao.selectAllTvs();
		assertNotNull(tvs);
		assertEquals(5, tvs.size());

		for (int i = 0; i < 5; i++) {
			TvDto tv = tvs.get(0);
			assertNotNull(tv);

			tvs = tvDao.selectAllTvs();
			assertEquals(5 - i, tvs.size());

			tvDao.delete(tv.getId());

			tvs = tvDao.selectAllTvs();
			assertEquals(4 - i, tvs.size());
		}
	}

	private void addPersistentTvs() {
		TvDto tv1 = new TvDto();
		tv1.setId(1L);
		tv1.setBrandName("brand1");
		tv1.setDeviceName("device1");
		tv1.setTypeNumber("type1");
		tv1.setSoftwareVersion("111.aaa");
		tv1.setYear(2001);
		tv1.setInteractive(true);
		tvDao.persist(tv1);

		TvDto tv2 = new TvDto();
		tv2.setId(2L);
		tv2.setBrandName("brand2");
		tv2.setDeviceName("device1");
		tv2.setTypeNumber("type1");
		tv2.setSoftwareVersion("222.aaa");
		tv2.setYear(1995);
		tv2.setInteractive(true);
		tvDao.persist(tv2);

		TvDto tv3 = new TvDto();
		tv3.setId(3L);
		tv3.setBrandName("brand2");
		tv3.setDeviceName("device2");
		tv3.setTypeNumber("type1");
		tv3.setSoftwareVersion("333.bbb-df");
		tv3.setYear(2011);
		tv3.setInteractive(false);
		tvDao.persist(tv3);

		TvDto tv4 = new TvDto();
		tv4.setId(4L);
		tv4.setBrandName("brand4");
		tv4.setDeviceName("device4");
		tv4.setTypeNumber("type4");
		tv4.setSoftwareVersion("444.bb-as");
		tv4.setYear(2003);
		tv4.setInteractive(true);
		tvDao.persist(tv4);

		TvDto tv5 = new TvDto();
		tv5.setId(5L);
		tv5.setBrandName("brand2");
		tv5.setDeviceName("device1");
		tv5.setTypeNumber("type1");
		tv5.setSoftwareVersion("55.aaa");
		tv5.setYear(2009);
		tv5.setInteractive(false);
		tvDao.persist(tv5);
	}

	private void deletePersistentTvs() {
		List<TvDto> tvs = tvDao.selectAllTvs();
		for (TvDto tv : tvs) {
			tvDao.delete(tv.getId());
		}
	}
}