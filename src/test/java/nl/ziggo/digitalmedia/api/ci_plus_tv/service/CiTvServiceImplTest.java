package nl.ziggo.digitalmedia.api.ci_plus_tv.service;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import nl.ziggo.digitalmedia.api.ci_plus_tv.model.tv_v10.ObjectFactory;
import nl.ziggo.digitalmedia.api.ci_plus_tv.model.tv_v10.Tvs;
import nl.ziggo.digitalmedia.api.ci_plus_tv.persistence.TvDto;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CiTvServiceImplTest extends XMLTestCase {
	ClassPathXmlApplicationContext context = null;

	private CiTvServiceImpl tvService;

	@Before
	public void setUp() {
		String[] contextFiles = new String[] {
				"applicationContext.xml"
		};
		context = new ClassPathXmlApplicationContext(contextFiles);
		assertNotNull(context);

		tvService = (CiTvServiceImpl) context.getBean("ciTvService");
		assertNotNull(tvService);
	}

	@Test
	public void testAllTvs() throws Exception {
		List<TvDto> dbTvs = createTvs();
		Tvs xmlTvs = tvService.createTvsXml(new ObjectFactory(), dbTvs);
		assertEquals(5, xmlTvs.getTv().size());
		JAXBContext jaxbContext = JAXBContext.newInstance("nl.ziggo.digitalmedia.api.ci_plus_tv.model.tv_v10");
		Marshaller marshaller = jaxbContext.createMarshaller();
		StringWriter writer = new StringWriter();
		marshaller.marshal(xmlTvs, writer);
		String responseXml = "";
		responseXml = writer.getBuffer().toString();
		Reader tvReader = new StringReader(responseXml);
		InputStream expectedInput = CiTvServiceImplTest.class.getResourceAsStream("/ci_plus_tv/tvs-all.xml");
		Reader expectedXml = new InputStreamReader(expectedInput);
		Diff differences = new Diff(expectedXml, tvReader);
		DetailedDiff detailedDifferences = new DetailedDiff(differences);
//		assertTrue(detailedDifferences.similar());
//		assertTrue(detailedDifferences.identical());
		assertXMLEqual(detailedDifferences, true);
	}

	private List<TvDto> createTvs() {
		List<TvDto> tvs = new ArrayList<TvDto>();
		TvDto tv1 = new TvDto();
		tv1.setId(1L);
		tv1.setBrandName("brand1");
		tv1.setDeviceName("device1");
		tv1.setTypeNumber("type1");
		tv1.setSoftwareVersion("111.aaa");
		tv1.setYear(2001);
		tv1.setInteractive(true);
		tvs.add(tv1);

		TvDto tv2 = new TvDto();
		tv2.setId(2L);
		tv2.setBrandName("brand2");
		tv2.setDeviceName("device1");
		tv2.setTypeNumber("type1");
		tv2.setSoftwareVersion("222.aaa");
		tv2.setYear(1995);
		tv2.setInteractive(true);
		tvs.add(tv2);

		TvDto tv3 = new TvDto();
		tv3.setId(3L);
		tv3.setBrandName("brand2");
		tv3.setDeviceName("device2");
		tv3.setTypeNumber("type1");
		tv3.setSoftwareVersion("333.bbb-df");
		tv3.setYear(2011);
		tv3.setInteractive(false);
		tvs.add(tv3);

		TvDto tv4 = new TvDto();
		tv4.setId(4L);
		tv4.setBrandName("brand4");
		tv4.setDeviceName("device4");
		tv4.setTypeNumber("type4");
		tv4.setSoftwareVersion("444.bb-as");
		tv4.setYear(2003);
		tv4.setInteractive(true);
		tvs.add(tv4);

		TvDto tv5 = new TvDto();
		tv5.setId(5L);
		tv5.setBrandName("brand2");
		tv5.setDeviceName("device1");
		tv5.setTypeNumber("type1");
		tv5.setSoftwareVersion("55.aaa");
		tv5.setYear(2009);
		tv5.setInteractive(false);
		tvs.add(tv5);

		return tvs;
	}
}