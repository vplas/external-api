package nl.ziggo.digitalmedia.api.ci_plus_tv.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="citv_tv")
public class TvDto {
	private long id;
	private String brand;
	private String device;
	private String typeNumber;
	private String softwareVersion;
	private int year;
	private boolean isInteractive;

	@TableGenerator(name="acc_citv_seq",
			initialValue = 0,
			allocationSize = 1,
			table = "core_generated_id",
			pkColumnName = "entity_name",
			pkColumnValue = "next_id",
			valueColumnName = "citv_id")
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "acc_citv_seq")
	public long getId() {
		return id;
	}

	public void setId(long number) {
		id = number;
	}

	@Column(name = "brand")
	public String getBrandName() {
		return brand;
	}

	public void setBrandName(String name) {
		brand = name;
	}

	@Column(name = "device")
	public String getDeviceName() {
		return device;
	}

	public void setDeviceName(String name) {
		device = name;
	}

	@Column(name = "type")
	public String getTypeNumber() {
		return typeNumber;
	}

	public void setTypeNumber(String number) {
		typeNumber = number;
	}

	@Column(name = "software_version")
	public String getSoftwareVersion() {
		return softwareVersion;
	}

	public void setSoftwareVersion(String version) {
		softwareVersion = version;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int number) {
		year = number;
	}

	@Column(name = "is_interactive")
	public boolean isInteractive() {
		return isInteractive;
	}

	public void setInteractive(boolean flag) {
		isInteractive = flag;
	}
}