package nl.ziggo.digitalmedia.api.ci_plus_tv.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import nl.ziggo.digitalmedia.api.ci_plus_tv.model.tv_v10.Tvs;

@Path("/ci_plus_tv")
public interface CiTvService {
	/**
	 * 
	 */
	@Path("/v1.0")
	@GET
	@Produces(MediaType.APPLICATION_XML)
	String version();

	/**
	 * Return all TVs.
	 */
	@GET
	@Path("/tvs/v1.0")
	@Produces(MediaType.APPLICATION_XML)
	Tvs getBrandNames();
}