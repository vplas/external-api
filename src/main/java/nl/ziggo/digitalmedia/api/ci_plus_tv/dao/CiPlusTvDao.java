package nl.ziggo.digitalmedia.api.ci_plus_tv.dao;

import java.util.List;

import nl.ziggo.digitalmedia.api.ci_plus_tv.persistence.TvDto;

public interface CiPlusTvDao {
	/**
	 * Return all CI+ TV's.
	 */
	List<TvDto> selectAllTvs();

	/**
	 * Persist the given tv.
	 */
	void persist(TvDto tv);

	/**
	 * Remove the given tv.
	 */
	void delete(long id);
}