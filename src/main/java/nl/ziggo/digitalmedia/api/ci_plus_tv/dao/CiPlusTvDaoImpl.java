package nl.ziggo.digitalmedia.api.ci_plus_tv.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import nl.ziggo.digitalmedia.api.ci_plus_tv.persistence.TvDto;

public class CiPlusTvDaoImpl implements CiPlusTvDao {
	@PersistenceUnit(unitName = "mysqlPersistence")
	private EntityManagerFactory entityManagerFactory;

	public List<TvDto> selectAllTvs() {
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		CriteriaBuilder criteria = entityManager.getCriteriaBuilder();
		CriteriaQuery<TvDto> criteriaQuery = criteria.createQuery(TvDto.class);
		Root<TvDto> tv = criteriaQuery.from(TvDto.class);
		criteriaQuery.select(tv);
		criteriaQuery.orderBy(criteria.asc(tv.get("brandName")), criteria.desc(tv.get("year")));

		TypedQuery<TvDto> query = entityManager.createQuery(criteriaQuery);
		List<TvDto> tvs = query.getResultList();

		entityManager.close();

		return tvs;
	}

	/**
	 * {@inheritDoc}
	 */
	public void persist(TvDto tv) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		entityManager.getTransaction().begin();

		entityManager.persist(tv);

		entityManager.getTransaction().commit();
		entityManager.close();
	}

	/**
	 * Delete the client with the given id from the database.
	 */
	public void delete(long id) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		TvDto tv = entityManager.find(TvDto.class, id);
		if (tv != null) {
			entityManager.getTransaction().begin();

			entityManager.remove(tv);

			entityManager.getTransaction().commit();
		}

		entityManager.close();
	}

	public void setEntityManagerFactory(EntityManagerFactory factory) {
		entityManagerFactory = factory;
	}
}