package nl.ziggo.digitalmedia.api.ci_plus_tv.service;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import nl.ziggo.digitalmedia.api.ci_plus_tv.dao.CiPlusTvDao;
import nl.ziggo.digitalmedia.api.ci_plus_tv.model.tv_v10.ObjectFactory;
import nl.ziggo.digitalmedia.api.ci_plus_tv.model.tv_v10.Tv;
import nl.ziggo.digitalmedia.api.ci_plus_tv.model.tv_v10.Tvs;
import nl.ziggo.digitalmedia.api.ci_plus_tv.persistence.TvDto;

//@Stateless
//@Default
public class CiTvServiceImpl implements CiTvService {
//	@Inject
//	@Autowired
	private CiPlusTvDao tvDao;

	/**
	 * test
	 */
	@Path("/v1.0")
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public String version() {
		return "<xml>test</xml>";
	}

	/**
	 * Return all TVs.
	 */
	@GET
	@Path("/tvs/v1.0")
	@Produces(MediaType.APPLICATION_XML)
	public Tvs getBrandNames() {
		ObjectFactory factory = new ObjectFactory();

		List<TvDto> dbTvs = tvDao.selectAllTvs();

		return createTvsXml(factory, dbTvs);
	}

	/**
	 * Return an XML representation for the given tv's.
	 */
	Tvs createTvsXml(ObjectFactory factory, List<TvDto> dbTvs) {
		Tvs xmlTvs = factory.createTvs();

		for (TvDto dbTv : dbTvs) {
			Tv xmlTv = factory.createTv();
			xmlTv.setBrandName(dbTv.getBrandName());
			xmlTv.setModel(dbTv.getDeviceName());
			xmlTv.setType(dbTv.getTypeNumber());
			xmlTv.setYear(Integer.toString(dbTv.getYear()));
			xmlTv.setSoftwareVersion(dbTv.getSoftwareVersion());
//			xmlTv.setCiVersion(dbTv.get);
			xmlTv.setIsInteractive(dbTv.isInteractive());

			xmlTvs.getTv().add(xmlTv);
		}

		return xmlTvs;
	}

	/**
	 * Return all known brands.
	 */
//	@GET
//	@Path("/brands/v1.0")
//	public Brands getBrandNames() {
//		
//	}

	/**
	 * Return all devices of the given brand.
	 */
//	@GET
//	@Path("/devices/brand/{brandName}/v1.0")
//	public Devices getDevicesByBrand(String brandName) {
//		
//	}

	/**
	 * Return all type numbers of the given brand.
	 */
//	@GET
//	@Path("/types/brand/{brandName}/v1.0")
//	public TypeNumbers getTypesByBrand(String brandName) {
//		
//	}

	public void setTvDao(CiPlusTvDao dao) {
		tvDao = dao;
	}
}