package nl.ziggo.digitalmedia.api.common.persistence;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="acc_client")
public class Client {
	private long id;
	private String name;
	private String code;
	private String secret;
	private String sourceIpAddress;

	@TableGenerator(name="acc_client_seq",
			initialValue = 0,
			allocationSize = 1,
			table = "core_generated_id",
			pkColumnName = "entity_name",
			pkColumnValue = "next_id",
			valueColumnName = "client_id")
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "acc_client_seq")
	public long getId() {
		return id;
	}

	public void setId(long number) {
		id = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String text) {
		name = text;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String text) {
		code = text;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String text) {
		secret = text;
	}

	public String getSourceIpAddress() {
		return sourceIpAddress;
	}

	public void setSourceIpAddress(String ipAddress) {
		sourceIpAddress = ipAddress;
	}
}