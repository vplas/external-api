package nl.ziggo.digitalmedia.api.common.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="acc_path")
public class AccessPath {
	private long id;
	private String clientCode;
	private String path;

	@TableGenerator(name="acc_path_seq",
			initialValue = 0,
			allocationSize = 1,
			table = "core_generated_id",
			pkColumnName = "entity_name",
			pkColumnValue = "next_id",
			valueColumnName = "access_path_id")
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "acc_path_seq")
	public long getId() {
		return id;
	}

	public void setId(long number) {
		id = number;
	}

	@Column(name = "client_code")
	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String code) {
		clientCode = code;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String accessPath) {
		path = accessPath;
	}
}