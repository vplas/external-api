package nl.ziggo.digitalmedia.api.common.dao;

import nl.ziggo.digitalmedia.api.common.persistence.AccessPath;
import nl.ziggo.digitalmedia.api.common.persistence.Client;
import nl.ziggo.digitalmedia.api.common.persistence.Token;


public interface ClientAccessDao {
	/**
	 * Persist the given client.
	 */
	void persist(Client client);

	/**
	 * Delete all clients.
	 */
	void deleteAllClients();

	/**
	 * Return true if the combination of the given client code and the given ip address exists.
	 */
	boolean existClient(String clientCode, String ipAddress);

	/**
	 * Return the secret for the given client.
	 */
	String retrieveSecretByClient(String clientCode);

	/**
	 * Delete all tokens.
	 */
	void deleteAllTokens();

	/**
	 * Persist the given token.
	 */
	void persist(Token token);

	/**
	 * Fetch the persistent token with the given client and given content.
	 */
	Token fetchTokenByClient(String clientCode, String tokenText);

	/**
	 * Persist the given access path.
	 */
	void persist(AccessPath paccessPath);

	/**
	 * Delete all access paths.
	 */
	void deleteAllAccessPaths();

	/**
	 * Return true if the combination of the given client and the given path exists.
	 */
	boolean existAccessPath(String clientCode, String path);
}