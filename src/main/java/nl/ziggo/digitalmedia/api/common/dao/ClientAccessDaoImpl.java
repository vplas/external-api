package nl.ziggo.digitalmedia.api.common.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import nl.ziggo.digitalmedia.api.common.persistence.AccessPath;
import nl.ziggo.digitalmedia.api.common.persistence.Client;
import nl.ziggo.digitalmedia.api.common.persistence.Token;

public class ClientAccessDaoImpl implements ClientAccessDao {
	@PersistenceUnit(unitName = "mysqlPersistence")
	private EntityManagerFactory entityManagerFactory;

	/**
	 * Return all clients from the database.
	 */
	private List<Client> selectAllClients() {
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		CriteriaBuilder criteria = entityManager.getCriteriaBuilder();
		CriteriaQuery<Client> criteriaQuery = criteria.createQuery(Client.class);
		Root<Client> client = criteriaQuery.from(Client.class);
		criteriaQuery.select(client);

		TypedQuery<Client> query = entityManager.createQuery(criteriaQuery);
		List<Client> clients = query.getResultList();

		entityManager.close();

		return clients;
	}

	/**
	 * {@inheritDoc}
	 */
	public void persist(Client client) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		entityManager.getTransaction().begin();

		entityManager.persist(client);

		entityManager.getTransaction().commit();
		entityManager.close();
	}

	/**
	 * Delete the client with the given id.
	 */
	private void deleteClient(long id) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		Client client = entityManager.find(Client.class, id);
		if (client != null) {
			entityManager.getTransaction().begin();

			entityManager.remove(client);

			entityManager.getTransaction().commit();
		}
		entityManager.close();
	}

	/**
	 * {@inheritDoc}
	 */
	public void deleteAllClients() {
		List<Client> clients = selectAllClients();
		for (Client client : clients) {
			deleteClient(client.getId());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean existClient(String clientCode, String ipAddress) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		CriteriaBuilder criteria = entityManager.getCriteriaBuilder();
		CriteriaQuery<Client> criteriaQuery = criteria.createQuery(Client.class);
		Root<Client> client = criteriaQuery.from(Client.class);
		criteriaQuery.select(client);
		criteriaQuery.where(criteria.equal(client.get("code"), clientCode));
		criteriaQuery.where(criteria.equal(client.get("sourceIpAddress"), ipAddress));

		TypedQuery<Client> query = entityManager.createQuery(criteriaQuery);
		List<Client> clients = query.getResultList();

		return clients.size() > 0;
	}

	/**
	 * {@inheritDoc}
	 */
	public String retrieveSecretByClient(String clientCode) {
		String secret = null;

		EntityManager entityManager = entityManagerFactory.createEntityManager();
		CriteriaBuilder criteria = entityManager.getCriteriaBuilder();
		CriteriaQuery<Client> query = criteria.createQuery(Client.class);
		Root<Client> client = query.from(Client.class);
		query.select(client);
		query.where(criteria.equal(client.get("code"), clientCode));

		TypedQuery<Client> executer = entityManager.createQuery(query);
		Client foundClient = null;
		try {
			foundClient = executer.getSingleResult();

			if (foundClient != null) {
				secret = foundClient.getSecret();
			}
		} catch (NoResultException exception) {
		} catch (NonUniqueResultException exception) {
		}

		return secret;
	}

	/**
	 * Return all tokens from the database.
	 */
	private List<Token> selectAllTokens() {
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		CriteriaBuilder criteria = entityManager.getCriteriaBuilder();
		CriteriaQuery<Token> criteriaQuery = criteria.createQuery(Token.class);
		Root<Token> token = criteriaQuery.from(Token.class);
		criteriaQuery.select(token);

		TypedQuery<Token> query = entityManager.createQuery(criteriaQuery);
		List<Token> tokens = query.getResultList();

		entityManager.close();

		return tokens;
	}

	/**
	 * {@inheritDoc}
	 */
	public void persist(Token token) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		entityManager.getTransaction().begin();

		entityManager.persist(token);

		entityManager.getTransaction().commit();
		entityManager.close();
	}

	/**
	 * Delete the token with the given id.
	 */
	private void deleteToken(long id) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		Token token = entityManager.find(Token.class, id);
		if (token != null) {
			entityManager.getTransaction().begin();

			entityManager.remove(token);

			entityManager.getTransaction().commit();
		}
		entityManager.close();
	}

	/**
	 * {@inheritDoc}
	 */
	public void deleteAllTokens() {
		for (Token token : selectAllTokens()) {
			deleteToken(token.getId());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public Token fetchTokenByClient(String clientCode, String tokenText) {
		Token foundToken = null;

		EntityManager entityManager = entityManagerFactory.createEntityManager();
		CriteriaBuilder criteria = entityManager.getCriteriaBuilder();
		CriteriaQuery<Token> query = criteria.createQuery(Token.class);
		Root<Token> token = query.from(Token.class);
		query.select(token);
		query.where(criteria.equal(token.get("clientCode"), clientCode));
		query.where(criteria.equal(token.get("token"), tokenText));

		TypedQuery<Token> executer = entityManager.createQuery(query);
		try {
			foundToken = executer.getSingleResult();
		} catch (NoResultException exception) {
		} catch (NonUniqueResultException exception) {
		}

		return foundToken;
	}

	/**
	 * Return all access paths from the database.
	 */
	private List<AccessPath> selectAllAccessPaths() {
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		CriteriaBuilder criteria = entityManager.getCriteriaBuilder();
		CriteriaQuery<AccessPath> criteriaQuery = criteria.createQuery(AccessPath.class);
		Root<AccessPath> accessPath = criteriaQuery.from(AccessPath.class);
		criteriaQuery.select(accessPath);

		TypedQuery<AccessPath> query = entityManager.createQuery(criteriaQuery);
		List<AccessPath> accessPaths = query.getResultList();

		entityManager.close();

		return accessPaths;
	}

	/**
	 * {@inheritDoc}
	 */
	public void persist(AccessPath accessPath) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		entityManager.getTransaction().begin();

		entityManager.persist(accessPath);

		entityManager.getTransaction().commit();
		entityManager.close();
	}

	/**
	 * Delete the access path with the given id.
	 */
	private void deleteAccessPath(long id) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		AccessPath accessPath = entityManager.find(AccessPath.class, id);
		if (accessPath != null) {
			entityManager.getTransaction().begin();

			entityManager.remove(accessPath);

			entityManager.getTransaction().commit();
		}
		entityManager.close();
	}

	/**
	 * {@inheritDoc}
	 */
	public void deleteAllAccessPaths() {
		for (AccessPath accessPath : selectAllAccessPaths()) {
			deleteAccessPath(accessPath.getId());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean existAccessPath(String clientCode, String path) {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		CriteriaBuilder criteria = entityManager.getCriteriaBuilder();
		CriteriaQuery<AccessPath> query = criteria.createQuery(AccessPath.class);
		Root<AccessPath> accessPath = query.from(AccessPath.class);
		query.select(accessPath);
		query.where(criteria.equal(accessPath.get("clientCode"), clientCode));
		query.where(criteria.equal(accessPath.get("path"), path));

		TypedQuery<AccessPath> executer = entityManager.createQuery(query);
		List<AccessPath> foundAccessPaths = executer.getResultList();

		return foundAccessPaths.size() > 0;
	}

	public void setEntityManagerFactory(EntityManagerFactory factory) {
		entityManagerFactory = factory;
	}
}