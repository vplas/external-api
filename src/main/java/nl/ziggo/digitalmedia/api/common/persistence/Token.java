package nl.ziggo.digitalmedia.api.common.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="acc_token")
public class Token {
	private long id;
	private String clientCode;
	private long timestamp; // number of seconds passed since the epoch
	private long lastAccessTime; // in seconds
	private String token;

	@TableGenerator(name="acc_token_seq",
			initialValue = 0,
			allocationSize = 1,
			table = "core_generated_id",
			pkColumnName = "entity_name",
			pkColumnValue = "next_id",
			valueColumnName = "token_id")
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "acc_token_seq")
	public long getId() {
		return id;
	}

	public void setId(long number) {
		id = number;
	}

	@Column(name = "client_code")
	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String code) {
		clientCode = code;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long time) {
		timestamp = time;
	}

	@Column(name = "last_access_time")
	public long getLastAccessTime() {
		return lastAccessTime;
	}

	public void setLastAccessTime(long time) {
		lastAccessTime = time;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String text) {
		token = text;
	}
}