package nl.ziggo.digitalmedia.api.common.security;

public interface AccessService {
	/**
	 * Generate a token for the client and return it.
	 */
	String generateToken(String path, String sourceAddress);

	/**
	 * Return true if the client has access to the have executed the given call.
	 */
	boolean checkAccess(String path, String sourceAddress);
}