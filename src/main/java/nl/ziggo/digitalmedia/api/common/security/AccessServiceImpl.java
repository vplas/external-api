package nl.ziggo.digitalmedia.api.common.security;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import nl.ziggo.digitalmedia.api.common.dao.ClientAccessDao;
import nl.ziggo.digitalmedia.api.common.persistence.Token;

import org.apache.commons.lang3.RandomStringUtils;

/*
 * BR: timestamp to generate token must not be in the future
 * BR: timestamp to generate token must not be older than TIMESTAMP_MAX_AGE seconds
 * BR: token cannot be used more than TOKEN_MAX_AGE seconds.
 * 
 * notes: timestamp in seconds (UNIX), java uses milliseconds
 * notes: signature contains capitals
 * notes: token contains TOKEN_LENGTH characters
 */
public class AccessServiceImpl implements AccessService {
	static final String ACCOUNT_PARAM_NAME = "account";
	static final String TIMESTAMP_PARAM_NAME = "timestamp";
	static final String TOKEN_PARAM_NAME = "token";
	static final String SIGNATURE_PARAM_NAME = "signature";
	static final String CONTEXT_PATH = "/rest/";

	// maximum number of seconds the timestamp is valid; 1 hour = 3600 seconds
	private static final long TIMESTAMP_MAX_AGE = 3600L;

	// maximum number of seconds the token is valid; 4 hours = 14.400 seconds 
	private static final long TOKEN_MAX_AGE = 14400L;

	// length of the token
	private static final int TOKEN_LENGTH = 64;

	private ClientAccessDao accessDao;

	/**
	 * {@inheritDoc}
	 */
	public String generateToken(String path, String sourceAddress) {
		String token = null;
		String clientCode = parseParameterValue(path, ACCOUNT_PARAM_NAME);
		String signature = parseParameterValue(path, SIGNATURE_PARAM_NAME);

		// check client and its source address
		boolean valid = checkSourceAddress(sourceAddress, clientCode);
		if (!valid) {
			// 403
		}

		// check timestamp
		String timestamp = parseParameterValue(path, TIMESTAMP_PARAM_NAME);
		valid = checkTimestamp(timestamp);
		if (!valid) {
			// 400
		}

		// hash client, timestamp and secret
		// compare hash result with client hash
		valid = checkSignature(clientCode, timestamp, signature);

		// generate token
		if (valid) {
			token = generateToken();
		}

		return token;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean checkAccess(String path, String sourceAddress) {
		boolean hasAccess = false;
		String clientCode = parseParameterValue(path, ACCOUNT_PARAM_NAME);
		String token = parseParameterValue(path, TOKEN_PARAM_NAME);

		// check client and its source address
		boolean valid = checkSourceAddress(sourceAddress, clientCode);
		if (!valid) {
			// 403
		}

		// check the generated token is still valid
		valid = checkToken(clientCode, token);
		if (!valid) {
			// 403
		}

		// check path to be executed by client
		valid = checkAccessToPath(path, clientCode);
		if (!valid) {
			// 403
		}

		return hasAccess;
	}

	/**
	 * Check the given timestamp in seconds.
	 */
	boolean checkTimestamp(String timestamp) {
		boolean valid = false;

		long clientTimestamp = 0L;
		try {
			clientTimestamp = Long.parseLong(timestamp);
		} catch (NumberFormatException exception) {
			// invalid timestamp given
		}

		if (clientTimestamp != 0L) {
			long serverTimestamp = System.currentTimeMillis() / 1000;

			// timestamp must no be in the future
			valid = serverTimestamp > clientTimestamp;

			// timestamp must not be older than one hour
			if (valid) {
				valid = serverTimestamp < clientTimestamp + TIMESTAMP_MAX_AGE;
			}
		}

		return valid;
	}

	/**
	 * Check the given signature.
	 */
	private boolean checkSignature(String clientCode, String timestamp, String signature) {
		boolean valid = false;

		StringBuilder path = new StringBuilder();
		path.append(ACCOUNT_PARAM_NAME);
		path.append('=');
		path.append(clientCode);
		path.append('&');
		path.append(TIMESTAMP_PARAM_NAME);
		path.append('=');
		path.append(timestamp);

		String secret = accessDao.retrieveSecretByClient(clientCode);
		if (secret != null) {
			path.append(secret);

			String createdSignature = createSignature(path.toString());
			valid = createdSignature.equals(signature);
		}

		return valid;
	}

	/**
	 * Create a signature for the given text.
	 */
	String createSignature(String text) {
		String signature = null;

		if (text != null) {
			try {
				byte[] textAsBytes = text.getBytes("UTF-8");

				MessageDigest digester = MessageDigest.getInstance("SHA-256");
				digester.update(textAsBytes);
				byte[] signatureAsBytes = digester.digest();

				signature = convertToHexString(signatureAsBytes);
			} catch (UnsupportedEncodingException exception) {
			} catch (NoSuchAlgorithmException exception) {
			}
		}

		return signature;
	}

	/**
	 * Convert each byte of the given byte array into a hexadecimal char. 
	 */
	private String convertToHexString(byte[] textBytes) {
		// symbols to use in output
		// warning: use upper case
		String hexSymbols = "0123456789ABCDEF";

		// convert each byte into 2 symbols
		StringBuilder hexText = new StringBuilder();
		for (byte textByte : textBytes) {
			hexText.append(hexSymbols.charAt((textByte & 0xF0) >> 4));
			hexText.append(hexSymbols.charAt((textByte & 0x0F)));
		}

		return hexText.toString();
	}

	/**
	 * Check the given token belongs to the given client.
	 * Check token is still valid.
	 */
	private boolean checkToken(String client, String tokenText) {
		boolean valid = false;

		// check the client has the given token
		Token token = accessDao.fetchTokenByClient(client, tokenText);
		if (token != null) {

			// check token has not expired and can be used to execute the call
			valid = !hasTokenExpired(token.getTimestamp());
		}

		return valid;
	}

	/**
	 * Return true if the given timestamp of a token has expired.
	 */
	boolean hasTokenExpired(long timestamp) {
		boolean hasExpired = true;

		long currentTimeSeconds = System.currentTimeMillis() / 1000;
		hasExpired = currentTimeSeconds > timestamp + TOKEN_MAX_AGE;

		return hasExpired;
	}

	/**
	 * Return true if the given client has access to execute the given path.
	 */
	private boolean checkAccessToPath(String path, String clientCode) {
		boolean hasAccess = false;

		hasAccess = accessDao.existAccessPath(clientCode, path);

		return hasAccess;
	}

	/**
	 * Generate a token of 64 characters and return it.
	 */
	String generateToken() {
		return RandomStringUtils.randomAlphanumeric(TOKEN_LENGTH);
	}

	/**
	 * Return true if the given ip address is registered with the given client.
	 */
	private boolean checkSourceAddress(String source, String client) {
		return accessDao.existClient(client, source);
	}

	/**
	 * Search the value of the given parameter within the given parameters and return it.
	 */
	String parseParameterValue(String urlPath, String parameterName) {
		String value = null;

		if (urlPath != null) {
			String searchText = parameterName + '=';

			int start = urlPath.indexOf(searchText);
			if (start != -1) {
				int end = urlPath.indexOf('&', start + searchText.length() + 1);
				if (end != -1) {
					value = urlPath.substring(start + searchText.length(), end);
				} else {
					value = urlPath.substring(start + searchText.length());
				}
			}
		}

		return value;
	}

	/**
	 * Find the call within the URL path and return it.
	 */
	String findCallFromPath(String path) {
		String call = null;

		if (path != null) {
			int start = path.indexOf(CONTEXT_PATH);
			if (start != -1) {
				call = path.substring(start + CONTEXT_PATH.length() - 1);
			}			
		}

		return call;
	}

	public void setAccessDao(ClientAccessDao dao) {
		accessDao = dao;
	}
}